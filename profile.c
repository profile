/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Matias Linares.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void spawn_child(char *progname, char **args, int fd);
void usage(char *progname);

void usage(char *progname)
{
    printf("%s [-o outfile] program arg1 arg2...\n"
           "\n"
           "Profile the program passed by parameter with it's arguments.\n"
           "If the [-o outfile] option is provided, the output of the program\n"
           "will be stored on output file. Otherwise it won't print any output\n",
           progname);
}

void spawn_child(char *progname, char **args, int fd)
{
    /* close the files descriptors. */
    if(fd > 0)
    {
        close(STDOUT_FILENO);
        dup(fd);
        close(STDERR_FILENO);
        dup(fd);

        close(fd);
    }
    else
    {
        close(STDERR_FILENO);
        close(STDOUT_FILENO);
    }

    if((execvp(progname, args)) == -1)
    {
        fprintf(stderr, "%d - %s\n", errno, strerror(errno));
        exit(1);
    }
}

int main(int argc, char **argv)
{
    pid_t curpid = 0;
    int status = -1;
    char *prog_name = NULL;
    char **args = NULL;
    char *out_file = NULL;
    int offset = 1;

    if(strcmp(argv[1], "-o") == 0)
    {
        out_file = (char *) calloc(strlen(argv[2]), sizeof(char));
        strcpy(out_file, argv[2]);
        offset += 2;
    }

    if(strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)
    {
        usage(argv[0]);
        return 0;
    }
    prog_name = (char *) calloc(strlen(argv[offset]), sizeof(char));
    strcpy(prog_name, argv[offset]);

    if(offset < argc)
        args = calloc(argc - offset + 1, sizeof(char*));

    /* The last parameter of the argv needs to be NULL. */
    for(int i = offset; i < argc; i++)
    {
        args[i - offset] = (char *) calloc(strlen(argv[i]) + 1, sizeof(char));
        strncpy(args[i - offset], argv[i], strlen(argv[i]));
    }
    args[argc - offset] = NULL;

    if((curpid = fork()) < 0)
    {
        perror("unable to fork()");
        exit(1);
    }

    if(curpid == 0)
    {
        int fd_out = -1;
        if(out_file)
        {
            /* Open the outfile with 664 permissions. */
            fd_out = open(out_file,
                          O_WRONLY | O_CREAT | O_TRUNC,
                          S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
        }
        spawn_child(prog_name, args, fd_out);
    }
    else
    {
        struct rusage usage;
        int who = RUSAGE_CHILDREN;

        waitpid(curpid, &status, 0);
        
        getrusage(who, &usage);

        printf("user CPU time used:                 %lus %luus\n", usage.ru_utime.tv_sec, usage.ru_utime.tv_usec);
        printf("system CPU time used:               %lus %luus\n", usage.ru_stime.tv_sec, usage.ru_stime.tv_usec);
        printf("residet set size used:              %ldKB\n", usage.ru_maxrss);
        printf("page reclaims (soft page faults):   %ld\n", usage.ru_minflt);
        printf("page faults (hard page faults):     %ld\n", usage.ru_majflt);
        printf("block input operations:             %ld\n", usage.ru_inblock);
        printf("block output operations:            %ld\n", usage.ru_oublock);
        printf("voluntary context switches:         %ld\n", usage.ru_nvcsw);
        printf("involuntary context switches:       %ld\n", usage.ru_nivcsw);

#ifdef _ENABLE_UNMANTAINED
        printf("integral shared memory size:        %ld\n", usage.ru_ixrss);
        printf("integral unshared data size:        %ld\n", usage.ru_idrss);
        printf("integral unshared stack size:       %ld\n", usage.ru_isrss);
        printf("swaps:                              %ld\n", usage.ru_nswap);
        printf("IPC messages sent:                  %ld\n", usage.ru_msgsnd);
        printf("IPC messages received:              %ld\n", usage.ru_msgrcv);
        printf("signals received:                   %ld\n", usage.ru_nsignals);
#endif
    }

    /* Clean up */
    if(prog_name)
        free(prog_name);
    for(int i = 0; i < argc - offset + 1; ++i)
        if(args[i] != NULL)
            free(args[i]);
    if(args)
        free(args);

    return status;
}
