# Profile

Profile a single program giving stats for CPU, memory and other things (see man
2 getrusage). The standard input is closed for the child process, so programs
like `cat` or `xargs` can't be profiled (for now).

The following list is the data that this software can tell you (on Linux
systems). The second list is all the features that are unmaintained for linux,
maybe on other systems it could be useful.

* user CPU time used
* system CPU time used
* maximum resident set size
* page reclaims (soft page faults)
* page faults (hard page faults)
* block input operations
* block output operations
* voluntary context switches
* involuntary context switches

### Unmaintained fields

* integral shared memory size
* integral unshared data size
* integral unshared stack size
* swaps
* IPC messages sent
* IPC messages received
* signals received

## TODO

* Express the CPU time on CPU cycles.
* Check if there are another function/library that could tell us more info about
  a child process.

## Example

    > ./profile thunar
    user CPU time used:                 0s 206666us
    system CPU time used:               0s 49999us
    residet set size used:              35864
    page reclaims (soft page faults):   3979
    page faults (hard page faults):     0
    block input operations:             0
    block output operations:            16
    voluntary context switches:         343
    involuntary context switches:       26

## Copyright

The MIT License (MIT)

Copyright (c) 2016 Matias Linares.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
