CC=clang
# Enable _ALLOW_UNMANTAINED for non Linux systems
CFLAGS=-Wall -Wextra -O0 -g # -D_ENABLE_UNMANTAINED
EXE=profile
SOURCES=profile.c

all:
	$(CC) $(CFLAGS) -o $(EXE) $(SOURCES) $(LDFLAGS)
